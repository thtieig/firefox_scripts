#/bin/bash

# This script can be set in a cron to clean the cache of a specific Firefox profile
# Handy if you have a test profile that you want to keep clean.
# Tested on Linux Debian

# Clean up Firfox profile
PROFILE_NAME="YOUR_PROFILE_NAME"

# Delete Cache
rm -rf ~/.cache/mozilla/firefox/$PROFILE_NAME/* 

# Delete History/stored stuffs
rm -rf ~/.mozilla/firefox/$PROFILE_NAME/*sqlite*

# Delete restore session (requires about:config -> browser.sessionstore.enabled false and browser.sessionstore.enabled false)
rm -rf ~/.mozilla/firefox/$PROFILE_NAME/sessionstore-backups/*
rm -rf ~/.mozilla/firefox/$PROFILE_NAME/sessionstore.js 

